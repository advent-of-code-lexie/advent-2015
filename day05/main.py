
def read_input(filename: str):
    with open(filename, "r") as file:
        return file.read().splitlines()

def validate_row(row: str) -> bool:
    if sum(row.count(vowel) for vowel in ['a', 'e', 'i', 'o', 'u']) < 3:
        return False
    prev_letter = None
    ok = False
    for letter in row:
        if letter == prev_letter:
            ok = True
            break
        prev_letter = letter
    if not ok:
        return False
    forbidden_occurrences = ['ab', 'cd', 'pq', 'xy']
    for occurrence in forbidden_occurrences:
        if row.find(occurrence) >= 0:
            return False
    return True

def part1(data: list):
    ok = 0
    for row in data:
        res = validate_row(row)
        if res:
            ok += 1
        print(row, res)
    print(ok)

input_data = read_input("input.txt")
part1(input_data)
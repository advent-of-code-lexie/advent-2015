use std::fs;

fn part1(contents: String) {

    let mut res = 0;
    for c in contents.chars() {
        if c == '(' {
            res += 1
        } else {
            res -= 1
        }
    }
    println!("Result: {res}");
}

fn part2(contents: String) {
    let mut calc = 0;
    for (i, c) in contents.chars().enumerate() {
        if c == '(' {
            calc += 1
        } else {
            calc -= 1
        }
        if calc < 0 {
            println!("Entering basement at position: {}", i + 1);
            return;
        }
    }
    println!("No basement level");
}


fn main() {
    // --snip--
    let file_path = "input.txt";

    println!("In file {}", file_path);

    let contents = fs::read_to_string(file_path)
        .expect("Should have been able to read the file");

    part2(contents);
}

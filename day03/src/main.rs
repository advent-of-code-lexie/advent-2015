use std::collections::HashSet;
use std::fs;


#[derive(Hash, Eq, PartialEq, PartialOrd)]
struct House {
    x: i32,
    y: i32
}

fn move_position(c: char, x: i32, y: i32) -> (i32, i32) {
    match c {
        '^' => (x, y + 1),
        '>' => (x + 1, y),
        'v' => (x, y - 1),
        '<' => (x - 1, y),
        _ => { println!("Problem"); return (0, 0); }
    }
}

fn part_1(contents: String) {
    let mut x = 0;
    let mut y = 0;
    let mut houses = vec![House { x, y }];
    for c in contents.chars() {
        (x, y) = move_position(c, x, y);
        houses.push(House { x, y });
    }

    let visited_houses = houses.iter().collect::<HashSet<_>>().iter().count();
    println!("Number of houses delivered : {}", visited_houses);
}

fn part_2(contents: String) {
    let mut s_x = 0;
    let mut s_y = 0;
    let mut rs_x = 0;
    let mut rs_y = 0;
    let mut houses = vec![House { x: 0, y: 0 }];
    for (i, c) in contents.chars().enumerate() {
        if i % 2 == 0 {
            (s_x, s_y) = move_position(c, s_x, s_y);
            houses.push(House { x: s_x, y: s_y });
        } else {
            (rs_x, rs_y) = move_position(c, rs_x, rs_y);
            houses.push(House { x: rs_x, y: rs_y });
        }
    }

    let visited_houses = houses.iter().collect::<HashSet<_>>().iter().count();
    println!("Number of houses delivered : {}", visited_houses);
}

fn main() {
    println!("Hello, world!");

    let filename = "inputs.txt";

    let contents = fs::read_to_string(filename).unwrap();

    // part_1(contents);
    part_2(contents);
}
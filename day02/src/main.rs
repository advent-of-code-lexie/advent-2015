use std::fs;
use std::str::Lines;

fn get_paper_quantity(l: i32, w: i32, h: i32) -> i32 {
    let lw = l * w;
    let lh = l * h;
    let wh = w * h;

    let binding = vec![lw, lh, wh];
    let min_dim = binding.iter().min().unwrap();

    return 2 * lw + 2 * lh + 2 * wh + min_dim
}

fn part_1(gifts: Lines) {
    let mut res = 0;
    for gift in gifts {
        let dimension: Vec<i32> = gift.split("x").map(|x| x.parse::<i32>().unwrap()).collect();

        res += get_paper_quantity(dimension[0], dimension[1], dimension[2]);
    }
    println!("Total gift paper needed: {}", res);
}

fn get_paper_quantity_2(l: i32, w: i32, h: i32) -> i32 {
    let lw = 2 * l + 2 * w;
    let lh = 2 * l + 2 * h;
    let wh = 2 * w + 2 * h;

    let binding = vec![lw, lh, wh];
    let min_perimeter = binding.iter().min().unwrap();

    return min_perimeter + l * h * w
}

fn part_2(gifts: Lines) {
    let mut res = 0;

    for gift in gifts {
        let dimension: Vec<i32> = gift.split("x").map(|x| x.parse::<i32>().unwrap()).collect();

        res += get_paper_quantity_2(dimension[0], dimension[1], dimension[2]);
    }
    println!("Total gift paper needed: {}", res);
}

fn main() {
    println!("Hello, world!");

    let file_path = "input.txt";

    println!("In file {}", file_path);

    let contents = fs::read_to_string(file_path)
        .expect("Should have been able to read the file");

    // part_1(contents);
    let gifts = contents.lines();

    // part_1(gifts);

    part_2(gifts);
}


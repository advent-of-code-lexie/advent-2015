import hashlib

def find_leading_zero_hash(secret_key: str, number_of_zeroes: int):
    index = 0
    while True:
        tmp = f'{secret_key}{index}'
        hashed = hashlib.md5(tmp.encode()).hexdigest()
        if hashed.startswith(number_of_zeroes * '0'):
            print(f'{index}: {tmp}')
            break
        index += 1

find_leading_zero_hash('yzbqklnj', 5)
find_leading_zero_hash('yzbqklnj', 6)
